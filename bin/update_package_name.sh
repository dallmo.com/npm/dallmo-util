#!/bin/bash

keyword="__package_name__"
package_name="$( cd .. ; basename $PWD | tr '.' "\n" | head -n 1 )"

function main_action {
  dir="$1"
  echo "updating dir : $dir"
  find "$dir" -type f -exec sed -i "s/$keyword/$package_name/" {} \;
  echo "done."
  echo
}

echo "package name : $package_name"
echo

main_action "../dist"
main_action "../test"

