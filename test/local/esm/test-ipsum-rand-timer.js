"usr strict";

import {timer} from "@dallmo/util/timer";
import {math as math_util} from '@dallmo/util/math';
import {ipsum as dallmo_ipsum} from "@dallmo/util/ipsum"; 

let rw, ri;

// loop it
while( true ){
  
  // random sentence in random length
  ri = math_util.rand_int( 2, 10 );
  rw = dallmo_ipsum.random_sentence( ri ); console.log(rw);
  
  // random timer wait
  ri = math_util.rand_int( 1, 3 );
    await timer.wait(ri);

}; // while true
