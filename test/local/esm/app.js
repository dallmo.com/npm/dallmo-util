"usr strict";

/*
console.log("math : ", math);
*/

// //import {util} from "@dallmo/util";
// import {math as math_util} from '@dallmo/util/math';
// // math_util.tester();

// import {string as string_util} from '@dallmo/util/string';
// string_util.tester();

// import {array as array_util} from '@dallmo/util/array';
// array_util.tester();

// import {timestamp} from '@dallmo/util/timestamp';
// import {tester as timestamp_tester} from '@dallmo/util/timestamp';
// //timestamp.tester();
// timestamp_tester();
// console.log( timestamp().full_ms );
// console.log( timestamp().full_ms );
// console.log( timestamp().full_ms );

// import {string as string_util} from '@dallmo/util/string';
// string_util.tester();

// const s1 = "qwerty";
// const s2 = string_util.head( s1,2 ); console.log("s2:", s2);
// const s3 = string_util.tail( s1,2 ); console.log("s3:", s3);

// import {ipsum as dallmo_ipsum} from "@dallmo/util/ipsum"; 

// dallmo_ipsum.tester();

// let rw;

// //rw = dallmo_ipsum.random_words(); console.log(rw);
// rw = dallmo_ipsum.random_sentence(20); console.log(rw);

import {timer} from "@dallmo/util/timer";
import {timestamp} from "@dallmo/util/timestamp";

console.log( timestamp().full_ms );
await timer.wait(2);
console.log( timestamp().full_ms );
await timer.wait_ms( 3000 );
console.log( timestamp().full_ms );
