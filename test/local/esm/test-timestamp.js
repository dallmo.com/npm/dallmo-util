"usr strict";

import {timer} from "@dallmo/util/timer";
import {timestamp} from "@dallmo/util/timestamp";

////////////////////////////////
async function wait_for( x ){

  console.log( "start time : ", timestamp().full_ms, "waiting for ", x, "seconds..." );
  await timer.wait(x);
  console.log( "end time : ", timestamp().full_ms );

}; // function wait_for
////////////////////////////////

await wait_for(2);
await wait_for(4);

