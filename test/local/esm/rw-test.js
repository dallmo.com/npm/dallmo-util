"use strict";

import {generate as rw} from "random-words";


const num_min_default = 5;
function random_words( num = num_min_default, separator=" " ){

  const r = rw({
    exactly: num,
    join: separator
  });

    return r;

}; // function random_words

//console.log( rw );
const rr = random_words();
console.log(rr);