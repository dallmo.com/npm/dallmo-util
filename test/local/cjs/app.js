"use strict";

( async()=>{


  // const util_math = await import("@dallmo/util/math");
  // util_math.math.tester();
  // util_math.tester();
  // const result = util_math.add_leading_zero(1,4);
  //       console.log("result: ", result );

  // using ESM-native module via dynamic import
  const dt = await import("@dallmo/util/timestamp");

  dt.tester();
  console.log( dt.timestamp().full_ms );
  console.log( dt.timestamp().full_ms );
  console.log( dt.timestamp().full_ms );
  
  const dallmo_timer = await import("@dallmo/util/timer");
  dallmo_timer.tester();
  console.log( dt.timestamp().full_ms );
    let x = 5;
    console.log("waiting for ", x, "seconds" ); 
    await dallmo_timer.wait(x);
  console.log( dt.timestamp().full_ms );
  //dallmo_timer.wait(); console.log( dt.timestamp().full_ms );
  //dallmo_timer.wait(); console.log( dt.timestamp().full_ms );

})(); // self-run async main
