# overview

- dist : package content published on npm ; 
- test : files / scripts for testing the package before publishing to npm ; 
  - via local installations
  - via jest
- archive : misc / dev / ref files ; 

