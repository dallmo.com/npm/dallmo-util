"use strict";

/**
 * @dallmo/util/string
 */

/////////////////////////////////////////////////////
function capitalize( _string ){

  return _string.charAt(0).toUpperCase() + _string.slice(1);

}; // function capitalize
const cap = capitalize;
/////////////////////////////////////////////////////
function head( string, n ){

  /**
   * return n char from string, start from the 1st char 
   */
  return string.slice( 0,n );

}; // function head
/////////////////////////////////////////////////////
function tail( string, n ){

  /**
   * return n char from string, start from the last char
   * but in the same sequence as the input string
   */
  return string.slice( -n );

}; // function tail
/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/util/string");

}; // function tester
/////////////////////////////////////////////////////
const string = {

  head,
  tail,
  cap,
  capitalize,

  tester,

}; // string
/////////////////////////////////////////////////////
export {

  head,
  tail,
  cap,
  capitalize,
  
  tester,
  string,
  
};
