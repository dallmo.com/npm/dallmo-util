"use strict";

/**
 * @dallmo/util/array
 */

import {math as math_util} from "@dallmo/util/math";

//////////////////////////////////////////////////////////////
function random_index( _array ){

  return Math.floor( Math.random() * _array.length );

}; // function random_index
const rand_index = random_index; // alias
//////////////////////////////////////////////////////////////
function random_item( _array ){

  return _array[ random_index( _array )];

}; // function random_item
const rand_item = random_item; // alias
//////////////////////////////////////////////////////////////
function random_insert( input_array, new_item ){

  //-----------------------------------------------------------------
  if( input_array.legnth == 0 ){
    return [new_item];
  }//if no item in the array

  //-----------------------------------------------------------------
  // define the returning array
  let new_array = [];
 
  // pick a random integer representing
  // the new item array index
  // in terms of length
  // if the current array is 0..N
  // then the new array is 0..N, N+1  
  // so, the new pos for this new item is between 0..N+1, inclusive
  // and N+1 equals to the lenght of the current array
  const new_item_index = math_util.random_integer( 0, input_array.length );
  
  //-----------------------------------------------------------------
  // 3 cases regarding new_item_index : 
  //  e.g. input array : [1,2,3]
  //        new array, possible cases
  /*
                  [a,1,2,3] : case 1
                  [1,a,2,3] : case 2
                  [1,2,a,3] : case 2
                  [1,2,3,a] : case 3
  */
  //  case 1 : new index is 0
  //  case 2 : new index equals to the length of current array
  //  case 3 : all other values in-between
  //-----------------------------------------------------------------
  if( new_item_index == 0 ){
    // case 1
    new_array = [new_item].concat( input_array );

  }else if( new_item_index == input_array.length ){
    // case 2
    new_array = input_array.slice();
    new_array.push( new_item );

  }else{
    // case 3
    const index_min  = 1; // i.e. skipped 0
    const index_max  = input_array.length -1;
    const index_rand = math_util.random_integer( index_min, index_max );

    new_array = insert_item( input_array, index_rand, new_item );

  }//if, 3 cases
  //-----------------------------------------------------------------
 
    return new_array;

}; // function random_insert
const rand_insert = random_insert;
//////////////////////////////////////////////////////////////
function insert_item( input_array, item_index, new_item ){

  // item_index is corresponding to input_array
  const input_array_head = input_array.slice( 0, item_index );
  const input_array_tail = input_array.slice( item_index, input_array.length );

  let temp_array_head = input_array_head.slice();
      temp_array_head.push( new_item );

    return temp_array_head.concat( input_array_tail );

}; // function insert_item
//////////////////////////////////////////////////////////////
function update_item( _array, _item_index, _new_item ){

  let new_array = [];
  _array.forEach( curr_item, curr_index => {
  
    if( curr_index == _item_index ){
      new_array.push( _new_item );
    }else{
      new_array.push( curr_item );
    }//if else, curr_index    

  });// forEach

  return new_array;

}; // function update_item
//////////////////////////////////////////////////////////////
function add_all( _array ){

  let sum = 0;

  try{
    // assume an array containing only integers
    // return the sum of all items
    _array.forEach( item => {
      sum += item;
    });
  }catch( e ){
    throw( e );
  }//try catch

  return sum;

}; // function add_all
const sum = add_all;
//////////////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/util/array")

}; // function tester
//////////////////////////////////////////////////////////////
const array = {

  random_index,
  rand_index,
  random_item,
  rand_item,
  random_insert,
  rand_insert,
  insert_item,
  update_item,
  add_all,
  sum,

  tester,

}; // array
//////////////////////////////////////////////////////////////
export {
  
  random_index,
  rand_index,
  random_item,
  rand_item,
  random_insert,
  rand_insert,
  insert_item,
  update_item,
  add_all,
  sum,

  tester,
  array,

};
