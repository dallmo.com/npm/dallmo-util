"use strict";

/**
 * @dallmo/util/
 */

/////////////////////////////////////////////////////
function tester(){

  console.log("tester message from : @dallmo/util.tester");

}; // function tester
/////////////////////////////////////////////////////
const util = {

  tester,

}; // util
/////////////////////////////////////////////////////
export {
  util,
  tester,
};
