"use strict";

/**
 * @dallmo/util/timer
 */

import {setTimeout} from "node:timers/promises";

const default_sleep_s  = 1;
const default_sleep_ms = 1000;

/////////////////////////////////////////////////////////////////////
async function wait_ms( sleep_interval = default_sleep_ms ){
  
  /**
   * wait for some milliseconds
   */
  
  await setTimeout( sleep_interval );
}; // wait
/////////////////////////////////////////////////////////////////////
async function wait_s( sleep_interval = default_sleep_s ){

  /**
   * wait for some seconds
   */

  await wait_ms( sleep_interval*1000 );
}; // function wait_s
const wait = wait_s;
/////////////////////////////////////////////////////////////////////
async function countdown(){

}; // countdown
/////////////////////////////////////////////////////////////////////
function tester(){
  console.log("tester message from : @dallmo/util/timer");
}; // function tester
/////////////////////////////////////////////////////////////////////
const timer = {
  
  wait,
  wait_s,
  wait_ms,
  countdown,
  
  tester,

}; // dallmo_connector
/////////////////////////////////////////////////////////////////////
export {
  
  wait,
  wait_s,
  wait_ms,
  countdown,
  
  tester,
  timer,

};// export
