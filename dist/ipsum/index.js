"use strict";

/**
 * @dallmo/util/ipsum
 */

import {math    as math_util}   from "@dallmo/util/math";
import {array   as array_util}  from "@dallmo/util/array";
import {string  as string_util} from "@dallmo/util/string";

import {generate as rw} from "random-words";

const num_min_default = 5;
const ending_char_array_default = [".","?","!"];

/////////////////////////////////////////////////////////////////////
function random_words( num = num_min_default, separator=" " ){

  const r = rw({
    exactly: num,
    join: separator
  });

    return r;

}; // function random_words
/////////////////////////////////////////////////////////////////////
function random_words_array( num = num_min_default ){

  const r = rw({
    exactly: num,
  });

    return r;

}; // function random_words_array
/////////////////////////////////////////////////////////////////////
function random_ending( _string, ending_char_array = ending_char_array_default ){

  let result_string = _string;

  const rand_ending_char = array_util.random_item( ending_char_array );
  result_string += rand_ending_char;

    return result_string;

}; // function random_ending
/////////////////////////////////////////////////////////////////////
function sentence_type( _string, _type ){
  /**
   * convert a given string into one of the sentence types
   * sentence type 1 : 
   *  - no comma
   * sentence type 2 : 
   *  - with comma
   * for all types : 
   *  - capital letter at the beginning
   *  - random "." or "?" at the end
   */

  try{
    switch( _type ){
      case 1: return sentence_type_1( _string );
      case 2: return sentence_type_2( _string );
      default: throw("unknown sentence type");
    }//switch 
  }catch(e){
    throw(e);
  };//try catch

}; // function sentence_type
/////////////////////////////////////////////////////////////////////
function sentence_type_1( _string ){

  let r;
      r = string_util.capitalize( _string );
      r = random_ending( r );

  return r;

}; // function sentence_type_1
/////////////////////////////////////////////////////////////////////
function sentence_type_2( _string ){

  // check : if there is no space, just return type 1
  //         when the char can't be found, indexOf will return -1
  const space_index = _string.indexOf(" ");
        if( space_index < 0 ){
          return sentence_type_1( _string );
        }//if 

  // move on : 
  //  1. split the space-separated string into an array of random words
  //  2. pick a random item except the last one
  const words_array = _string.split(" ");

    return words_array_to_sentence_type_2( words_array );

}; // function sentence_type_2
/////////////////////////////////////////////////////////////////////
function random_sentence( num = 5 ){

  const r = math_util.random_integer( 1, 2 );
  return random_sentence_type( r, num );

}; // function random_sentence
/////////////////////////////////////////////////////////////////////
function random_sentence_type( type, num = 5 ){

  switch( type ){
    case 1: return random_sentence_type_1( num );
    case 2: return random_sentence_type_2( num );
  }//switch

}; // function random_sentence_type
/////////////////////////////////////////////////////////////////////
function random_sentence_type_1( num = 5 ){

  let s;
      s = random_words( num );
      s = sentence_type_1( s );

      return s;

}; // function random_sentence_type_1
/////////////////////////////////////////////////////////////////////
function random_sentence_type_2( num = 5 ){

  const rand_words_array = random_words_array( num ); 
  
    return words_array_to_sentence_type_2( rand_words_array );

}; // function random_sentence_type_2
/////////////////////////////////////////////////////////////////////
function words_array_to_sentence_type_2( words_array ){

  const array_length = words_array.length;

  //----------------------------------------
  if( array_length == 1 ){
    return sentence_type_1( words_array[0] );
  }//if, array has only 1 item
  else if( array_length == 2 ){

    const a = words_array;
    const string_with_comma = string_util.capitalize(a[0]) + ", " + a[1];
    
    return random_ending( string_with_comma );

  }//else, if array has only 2 items
  //----------------------------------------
  // this is a temp array to trim off the 1st and last itme
  // for adding comma, so that the comma won't appear
  // at the beginning or just before the ending char, "." or "?"
  // slice( 1 ) : return a new array, with the first item skipped ; 
  // slice( 0, -1 ) : return a new array, with the last item skipped ; 
  const rand_words_body_only_array = words_array.slice( 1 ).slice( 0, -1 );

  // insert comma randomly
  let temp_comma_array = array_util.random_insert( rand_words_body_only_array, "," );
      temp_comma_array.push( words_array[ array_length - 1 ] ); // add the last item back
      temp_comma_array.unshift( words_array[0] ); // add the first item back ;

  const rand_words_with_comma_array = temp_comma_array.slice(); // clone it as a new array ;
  
  // capitalize the 1st word
  let word_1st;
      word_1st = rand_words_with_comma_array[0];
      word_1st = string_util.capitalize( word_1st );

  // replace the 1st word in rand_words_with_comma_array
  const with_comma_array = [word_1st].concat( rand_words_with_comma_array.slice(1) );   
  const string_by_joining_array = with_comma_array.join(" ");

  // remove leading space of comma by join
  const string_clean_comma = string_by_joining_array.replace(" , ", ", "); 
  const string_with_ending = random_ending( string_clean_comma );

    return string_with_ending;

}; // function words_array_to_sentence_type_2
/////////////////////////////////////////////////////////////////////
function tester(){
  console.log("tester message from : @dallmo/util/ipsum");
}; // function tester
/////////////////////////////////////////////////////////////////////
const ipsum = {
  
  random_words,
  random_ending,
  sentence_type,
  sentence_type_1,
  sentence_type_2,
  random_sentence,
  random_sentence_type,
  random_sentence_type_1,
  random_sentence_type_2,
  words_array_to_sentence_type_2,

  tester,
  
}; // dallmo_connector
/////////////////////////////////////////////////////////////////////
export {
  
  random_words,
  random_ending,
  sentence_type,
  sentence_type_1,
  sentence_type_2,
  random_sentence,
  random_sentence_type,
  random_sentence_type_1,
  random_sentence_type_2,
  words_array_to_sentence_type_2,
  
  tester,
  ipsum,

};// export
