# dallmo-util

- just some simple utils to be reused in other packages / codes ; 
- compatible with both cjs and esm, by following [the advice of a post by Dan Fabulich][ref-1] ;

[ref-1]: https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1

# usage

require / import the subpackage directly, then call the method comes with that object.

## subpackage list and methods ( alias )

- array_util
  - random_item
  - random_index
  - random_insert
  - insert_item
  - update_item
  - add_all ( sum )
  - tester

- math_util
  - add_leading_zero
  - random_integer ( random_int )
  - random_sections ( random_sec )
  - tester

- date_util
  - year ( YYYY )
  - month ( MM )
  - day ( DD )
  - hour ( hours, HH )
  - minute ( minutes, mm, min )
  - second ( seconds, ss, sec )
  - timestamp
  - tester

- string_util
  - head
  - tail
  - random_ending
  - random_words
  - sentence_type
  - sentence_type_1
  - sentence_type_2
  - random_sentence
  - random_sentence_type
  - random_sentence_type_1
  - random_sentence_type_2
  - words_array_to_sentence_type_2
  - capitalize
  - cap
  - tester

## cjs

to use the method, e.g., `random_item` in `array_util` : 

```
const array_util = require("dallmo-util/array");
```

## esm

```
import {array_util} from 'dallmo-util/array';
```

then, in either case, use the method with :

```
const result_item = array_util.random_item( some_array );
```


