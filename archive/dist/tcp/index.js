"use strict";

/*
  ----------------------------------------------------
  dallmo-util/tcp
  ----------------------------------------------------
*/

let log_msg;
const log_prefix = "util/tcp >> ";

///////////////////////////////////////////////////////////////////////////////
const socket_client_create = () => {

  // Create a new TCP client.
  const Net = require('net');
  const client = new Net.Socket();
  
  log_msg = log_prefix + "socket_client_create : client created."
  global.logger.info( log_msg );

    return client;

}; // socket_client_create
///////////////////////////////////////////////////////////////////////////////

const socket_client_destroy = async( client ) => {

  const r = await client.destroy();

  log_msg = log_prefix + "socket_client_destroy : connection ended.";
  global.logger.info( log_msg );
  
};//socket_client_destroy 
///////////////////////////////////////////////////////////////////////////////
const get_hex_raw = ( hex_string ) => {

  return Buffer.from( hex_string, 'hex');
  
}; // get_hex_raw
///////////////////////////////////////////////////////////////////////////////
// for multiple sending while connection is up
const send_data = async ( config_obj ) => {

  const client = config_obj.client;
  const data   = config_obj.data;

  try{
    log_msg = log_prefix + "send_data : sending : " + data ;
    global.logger.info( log_msg );
    client.write( data ); //This will send the byte buffer over TCP
  }catch(e){

    log_msg = log_prefix + "send_data : error : " + e ;
    global.logger.error( log_msg );

  }; // try catch

}; // send_data
///////////////////////////////////////////////////////////////////////////////
// create a connection, send once, close connection
const send_data_once = async ( config_obj ) => {

  const data = config_obj.data;

  try{
    
    const client = await socket_client_create();

    // Send a connection request to the server.
    await client.connect( config_obj, () => {
      // If there is no error, the server has accepted the request and created a new 
      // socket dedicated to us.
      log_msg = log_prefix + "send_data_once : connection started ...";
      global.logger.info( log_msg );

      send_data( client, data );
      socket_client_destroy( client );
    }); // client.connect

  }catch(e){
    
    log_msg = log_prefix + "send_data_once : error : " + e;
    global.logger.error( log_msg );

  }; // try catch

};// send_tcp
///////////////////////////////////////////////////////////////////////////////
const get_ip_from_request = ( request ) => {
  
  return request.ip.split(":").pop();

}; // get_ip_from_request
///////////////////////////////////////////////////////////////////////////////
// check if the incoming request originated from
// any of the localhost network addresses
const request_sent_from_local = ( request ) => {

    const request_ip = get_ip_from_request( request );

    // an array contains all ip of the localhost
    // to be checked against the incoming request ip
    const local_ip_array = Object.values(require("os").networkInterfaces())
                                  .flat()
                                  .filter(({ family, internal }) => family === "IPv4" && !internal)
                                  .map(({ address }) => address);
  
    // a boolean value
    //    true : if the ip making this request belongs to any localhost-related ip address
    const result = (( request_ip === "127.0.0.1" ) || ( local_ip_array.includes( request_ip )));
    
    return result;
  
  }; // request_from_local_address
///////////////////////////////////////////////////////////////////////////////
const log_incoming_request_skip_local = ( req ) => {

  //----------------------------------------------------------
  // local requests like refresh events, only goes to console
  // all others log to file and cloudwatch
  const request_ip = get_ip_from_request( req );
  const route = req.originalUrl;
  const log_msg = "route : " + route + ", remote_ip : " + request_ip;
  
  //depends on where the request is from, log it differently
  if( request_sent_from_local( req ) ){
    // do nothing
  }else{
    // log to file and remote log vault / cloudwatch
    global.logger.info( log_msg );
  }// filter local requests
  
  // in any case, log to console
  global.logger_console( log_msg );
  //----------------------------------------------------------

}; // log_request_to_route
///////////////////////////////////////////////////////////////////////////////
// all exports go here
module.exports = {
  socket_client_create,
  socket_client_destroy,
  get_hex_raw,
  send_data,
  send_data_once,
  get_ip_from_request,
  request_sent_from_local,
  log_incoming_request_skip_local,
};
