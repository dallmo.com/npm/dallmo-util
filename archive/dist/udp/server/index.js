"use strict";

/*
  util/udp/server
*/

let log_msg;
const log_prefix = "util/UDP/server >> ";

const dgram = require('dgram');
const server = dgram.createSocket('udp4');

// all functions called by the listeners go here
const util_udp = require("./util");

let message_handler;

/////////////////////////////////////////////////////////////////////
const start = ( config_obj ) => {

  const log_header = log_prefix + "start >> ";

  // for the UDP server
  const udp_server_port = config_obj.udp.server.port;

  // ==========================================
  // show message when the udp server has started listening
  server.on('listening', () => {
    util_udp.on_server_listening( server );
  }); // server - on listening

  // ==========================================
  // in case there is any server error
  server.on('error', (err) => {
    util_udp.on_server_error( server, err );
  });// server - on error

  // ==========================================
  // when incoming data is received
  server.on('message', ( buffer_in, remote_info ) => {

    //.......................................
    try{
      
      // just log the incoming message if no handler is found
      if( typeof( message_handler ) === "undefined" ){
        log_msg = log_header + "on message >> no message handler set >> "
                    + "message : " + buffer_in.toString() + ", "
                    + "remote ip : " + remote_info.address;
                    global.logger.info( log_msg );
      }else{
        // pass both the incoming message and remote info to the handler
        const data_in_obj = {
          message: buffer_in.toString(),
          buffer: buffer_in, 
          remote_info: remote_info,
        }; // data_in_obj
        message_handler( data_in_obj );
      }// if else, message_handler defined or not
  
    }catch(error){
      log_msg = log_header + "error : " + error.toString();
      global.logger.error( log_msg );
    }; // try catch error
    //.......................................

  });// server - on message received

  // ==========================================
  // start the udp server
  log_msg = log_header + "starting";
  global.logger.info( log_msg );

  server.bind( udp_server_port );

};
/////////////////////////////////////////////////////////////////////
// a custom function for dealing with the incoming message
const set_message_handler = ( custom_handler ) => {

  message_handler = custom_handler;

}; // set_message_handler
/////////////////////////////////////////////////////////////////////
// all exports go here
module.exports = {
  start,
  set_message_handler,
};

