"use strict";

/*
  util/udp/server/util
*/

let log_msg;
const log_prefix = "util/UDP/server >> ";

const util_misc = require("../../../misc");

/////////////////////////////////////////////////////////////////////
const on_server_listening = ( server ) => {

  const log_header = log_prefix + "on listening >> ";

  const server_address = server.address();
  const address = server_address.address
  const port = server_address.port;

  log_msg = log_header + "endpoint : " + address + ":" + port;
  global.logger.info( log_msg );

};// server - on start listening to incoming messages
/////////////////////////////////////////////////////////////////////
const on_server_error = ( server, err ) => {
  
  const log_header = log_prefix + "on error >> ";

  log_msg = log_header + err;
  global.logger.error( log_msg );

  server.close();
  global.logger.error("UDP server closed.");

};// server - on 
/////////////////////////////////////////////////////////////////////
// this should be defined along the server call 
// without changing anything here. included here only as a reference
const on_server_message = ( buffer_in, remote_info ) => {

  const log_header = log_prefix + "on message sample function >> ";

  // convert incoming buffer to string
  const udp_data_in = buffer_in.toString();

  // the message sent from
  const data_in_ip = remote_info.address;

      log_msg = log_header
                    + "incoming message : " + udp_data_in + ", "
                    + "remote ip : " + remote_info.address;
      global.logger.info( log_msg );

};// server - on 
/////////////////////////////////////////////////////////////////////
// all exports go here
module.exports = {
  on_server_listening,
  on_server_error,
  on_server_message,
};//



