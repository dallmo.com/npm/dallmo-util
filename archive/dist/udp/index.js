"use strict";

/*
  ----------------------------------------------------
  dallmo-util/udp
  ----------------------------------------------------
*/
let log_msg;
const log_prefix = "util/udp >> ";

///////////////////////////////////////////////////////////////////////////////
const client = require("./client");
const server = require("./server");
///////////////////////////////////////////////////////////////////////////////
module.exports = {
  client,
  server,
}; // module.exports
