"use strict";

/*
  util/udp/client
*/

let log_msg;
const log_prefix = "util/UDP/client >> ";

////////////////////////////////////////////////////////////////
// send udp message
const send_message = ( config_obj ) => {

  const message = config_obj.udp.message;
  const host    = config_obj.udp.server.host;
  const port    = config_obj.udp.server.port;

  const log_header = log_prefix + "send_message >> ";

  const dgram = require("dgram");
  const client = dgram.createSocket('udp4');
        client.send( message, 0, message.length, port, host, function(err, bytes) {
          if (err) {
            log_msg = log_header + "error: " + err;
              global.logger.error( log_msg );
          } else {
            log_msg = log_header + 
                        "message : " + message + ", " + 
                        "endpoint : " + host + ":" + port;
              global.logger.info( log_msg );
          }//if err
          client.close();
        }); // client.send

}; // send_message
////////////////////////////////////////////////////////////////
// all exports go here
module.exports = {
  send_message,
}; // module.exports

