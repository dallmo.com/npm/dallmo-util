"use strict";

/*
  ----------------------------------------------------
  dallmo-util/math
  ----------------------------------------------------
*/

//##################################################
// add leading zero, return as string
const add_leading_zero = ( num, total_length ) => {

  return String( num ).padStart( total_length, '0' );

};// add_leading_zero
//##################################################
// report a random integer between 2 nums, inclusive
const random_integer = ( _min, _max ) => {
  
  return Math.floor( _min + ( Math.random() * (_max - _min + 1) ) );

};//array_random_item
const rand_int = random_integer;
//##################################################
// given an input number ( num_input ),
// and the min and max value of each section,
// this function divide the input number into random sections,
// with the requirement that the sum of all sections 
// equal to the input number, 
// and return the result as an array
const random_sections = ( num_input, option_obj ) => {

  const num_min_default = 1;
  const num_max_default = 5;

  // the default values
  let num_min = num_min_default;
  let num_max = num_max_default;

  //------------------------------------------------------
  if( typeof( option_obj ) === "undefined" ){
    // ignore this case, 'cause default values were given above
  }else{
    num_min = option_obj.num_min || num_min_default;
    num_max = option_obj.num_max || num_max_default;
  }//if typeof

  // an alias to the method in this same package above
  const rand_int = random_integer;
  
  let rand_num_array = [];
  let counter = num_input;

  while( counter > 0 ){

    let num_sec_rand;

    //------------------------------------------------------
    if( counter < num_min * 2 ){
      num_sec_rand = counter ;
      counter = 0; // break the loop
    }// if
    else{

      if( counter < num_max ){
        num_sec_rand = rand_int( num_min, counter );
      }else{
        num_sec_rand = rand_int( num_min, num_max );
      };

      /*
        --------------------------------------------------
        in case, by subtracting num_sec_rand, 
        the resulting counter is smaller than num_min
        that just take the current counter as num_sec_rand
        this is to make sure the counter 
        will never smaller than num_min
        --------------------------------------------------
      */
      if( ( counter - num_sec_rand ) < num_min ){
        num_sec_rand = counter;
        counter = 0; // break the loop
      }else{
        counter -= num_sec_rand;
      }// if else

    }//else, counter >= num_min * 2

    rand_num_array.push( num_sec_rand );

    //------------------------------------------------------

  }//while loop

  return rand_num_array;

};// random_sections
const rand_sec = random_sections;
//##################################################
const tester = ( _msg = "default msg here" ) => {
  return "from math_util.tester : " + _msg ;
};// tester
//##################################################
// all exports goes here
module.exports = {
  add_leading_zero,
  random_integer,
  rand_int,
  random_sections,
  rand_sec,
  tester,
};

