/*
  a tiny wrapper for cjs to esm compatibility
  ref : https://redfin.engineering/node-modules-at-war-why-commonjs-and-es-modules-cant-get-along-9617135eeca1
*/

import dallmo_util from '../index.js';

export {
  dallmo_util
};

