"use strict";

const { setTimeout }  = require('node:timers/promises');

/////////////////////////////////////////
const wait = async ( sleep_interval = 1000 ) => {
  await setTimeout( sleep_interval );
}; // timeout
/////////////////////////////////////////
const countdown = async () => {
  
}; // countdown
/////////////////////////////////////////
module.exports = {
  wait,
  countdown,
};
