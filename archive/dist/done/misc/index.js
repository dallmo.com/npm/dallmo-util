"use strict";

/*
  ----------------------------------------------------
  dallmo-util/misc
  ----------------------------------------------------
*/

const timer = require("./timer");


module.exports = {
  timer,
};
