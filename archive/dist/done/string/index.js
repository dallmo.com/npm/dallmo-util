"use strict";

/*
  ----------------------------------------------------
  dallmo-util/string
  ----------------------------------------------------
*/

const array_util = require("../array/index.js");
const math_util = require("../math/index.js");
const rw = require("random-words");

const num_min_default = 5;
const ending_char_array_default = [".","?"];

//#############################################################
// return n char from string, start from the 1st char
const head = ( string, n ) => {

  return string.slice( 0,n );

}; // head
//#############################################################
// return n char from string, start from the last char
// but in the same sequence as the input string
const tail = ( string, n ) => {

  return string.slice( -n );

}; // tail
//#############################################################
// convert a given string into one of the sentence types
/*
  sentence type 1 : 
    - no comma
  
  sentence type 2 : 
    - with comma

  for all types : 
    - capital letter at the beginning
    - random "." or "?" at the end
*/
const sentence_type = ( _string, _type ) => {

  try{
    switch( _type ){
      case 1: return sentence_type_1( _string );
      case 2: return sentence_type_2( _string );
      default: throw("unknown sentence type");
    }//switch 
  }catch(e){
    throw(e);
  };//try catch

};// sentence_type
//#############################################################
// the string given contains random words, separated with spaces
// this function convert the string to sentence type 1
// sentence type 1 : no comma
const sentence_type_1 = ( _string ) => {

  let r;
      r = capitalize( _string );
      r = random_ending( r );

  return r;

};// sentence_type_1
//#############################################################
// the string given contains random words, separated with spaces
// this function convert the string to sentence type 2
// sentence type 2 : with comma
const sentence_type_2 = ( _string ) => {

  // check : if there is no space, just return type 1
  //         when the char can't be found, indexOf will return -1
  const space_index = _string.indexOf(" ");
        if( space_index < 0 ){
          return sentence_type_1( _string );
        }//if 

  // move on : 
  //  1. split the space-separated string into an array of random words
  //  2. pick a random item except the last one
  const words_array = _string.split(" ");

    return words_array_to_sentence_type_2( words_array );

};// sentence_type_2
//#############################################################
// append a char to the string,
//  picked from the default or given array
const random_ending = ( _string, ending_char_array = ending_char_array_default ) => {

  let result_string = _string;

  const rand_ending_char = array_util.random_item( ending_char_array );
  result_string += rand_ending_char;

    return result_string;

};// random_ending
//#############################################################
// a wrapper for the npm package random-words
// return a string joined from an array
const random_words = ( num = num_min_default, separator=" " ) => {

  const r = rw({
    exactly: num,
    join: separator
  });

    return r;

};//random_words
//#############################################################
// a wrapper for the npm package random-words
// just provide a num to get an array of random words
const random_words_array = ( num = num_min_default ) => {

   const r = rw({
    exactly: num,
  });

    return r;

};// random_words_array
//#############################################################
// this generate either type 1 or type 2 sentences
const random_sentence = ( num = 5 ) => {

  const r = math_util.random_integer( 1, 2 );
  return random_sentence_type( r, num );

};// random_sentence
//#############################################################
// this specify the type num
const random_sentence_type = ( type, num = 5 ) => {

  switch( type ){
    case 1: return random_sentence_type_1( num );
    case 2: return random_sentence_type_2( num );
  }//switch

};// random_sentence_type
//#############################################################
const random_sentence_type_1 = ( num = 5 ) => {

  let s;
      s = random_words( num );
      s = sentence_type_1( s );

      return s;

};// random_sentence_type_1
//#############################################################
const random_sentence_type_2 = ( num = 5 ) => {

  const rand_words_array = random_words_array( num ); 
  
    return words_array_to_sentence_type_2( rand_words_array );

};// random_sentence_type_2
//#############################################################
const words_array_to_sentence_type_2 = ( words_array ) => {
 
  const array_length = words_array.length;

  //----------------------------------------
  if( array_length == 1 ){
    return sentence_type_1( words_array[0] );
  }//if, array has only 1 item
  else if( array_length == 2 ){

    const a = words_array;
    const string_with_comma = capitalize(a[0]) + ", " + a[1];
    
    return random_ending( string_with_comma );

  }//else, if array has only 2 items
  //----------------------------------------
  // this is a temp array to trim off the 1st and last itme
  // for adding comma, so that the comma won't appear
  // at the beginning or just before the ending char, "." or "?"
  // slice( 1 ) : return a new array, with the first item skipped ; 
  // slice( 0, -1 ) : return a new array, with the last item skipped ; 
  const rand_words_body_only_array = words_array.slice( 1 ).slice( 0, -1 );

  // insert comma randomly
  let temp_comma_array = array_util.random_insert( rand_words_body_only_array, "," );
      temp_comma_array.push( words_array[ array_length - 1 ] ); // add the last item back
      temp_comma_array.unshift( words_array[0] ); // add the first item back ;

  const rand_words_with_comma_array = temp_comma_array.slice(); // clone it as a new array ;
  
  // capitalize the 1st word
  let word_1st;
      word_1st = rand_words_with_comma_array[0];
      word_1st = capitalize( word_1st );

  // replace the 1st word in rand_words_with_comma_array
  const with_comma_array = [word_1st].concat( rand_words_with_comma_array.slice(1) );   
  const string_by_joining_array = with_comma_array.join(" ");

  // remove leading space of comma by join
  const string_clean_comma = string_by_joining_array.replace(" , ", ", "); 
  const string_with_ending = random_ending( string_clean_comma );

    return string_with_ending;

};// words_array_to_sentence_type_2
//#############################################################
// this function only change the 1st char of a string to upper
const capitalize = ( _string ) => {
  
  return _string.charAt(0).toUpperCase() + _string.slice(1);

};// capitalize
const cap = capitalize;
///////////////////////////////////////////////////////////////
// borrowed from a util function in 2001-network-messenger
const trim_to_pos = ( data, pos ) => {

  return data.slice(0,pos);
  
};// trim_to_pos
///////////////////////////////////////////////////////////////
const tester = ( msg="default message" ) => {
  return "from string_util.tester : " + msg ;
};//tester
///////////////////////////////////////////////////////////////
// all exports goes here
module.exports = {
  head,
  tail,
  random_words,
  random_ending,
  sentence_type,
  sentence_type_1,
  sentence_type_2,
  random_sentence,
  random_sentence_type,
  random_sentence_type_1,
  random_sentence_type_2,
  words_array_to_sentence_type_2,
  capitalize,
  cap,
  trim_to_pos,
  tester,
};

