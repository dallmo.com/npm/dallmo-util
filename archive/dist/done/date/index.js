"use strict";

/*
  ----------------------------------------------------
  dallmo-util/date
  ----------------------------------------------------
*/

const math_util = require("../math/index.js");
const z = math_util.add_leading_zero;

//##################################################
const year = () => {

  const d = new Date();
  return d.getFullYear();

};//year
const YYYY = year;
//##################################################
const month = () => {

  const d = new Date();
  /*
    date.getMonth() : 
      0 : jan
      11 : dec
    ref : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getMonth
  */
  return z( d.getMonth()+1, 2 );

};//month
const MM = month;
//##################################################
const day = () => {

  const d = new Date();
  return z( d.getDate(), 2 );

};//day
const DD = day;
//##################################################
const hour = () => {

  const d = new Date();
  return z( d.getHours(), 2 );

};//hour
const HH = hour;
const hours = hour;
//##################################################
const minute = () => {

  const d = new Date();
  return z( d.getMinutes(), 2 );

};//minute
const mm = minute;
const min = minute;
const minutes = minute;
//##################################################
const second = () => {

  const d = new Date();
  return z( d.getSeconds(), 2 );

};//second
const ss = second;
const sec = second;
const seconds = second;
//##################################################
// get timestamp
const timestamp = ( separator = "-" ) => {

  const s = separator;
  return ( YYYY() + s + MM() + s + DD()
              + "_" + HH() + s + mm() + s + ss() );

};//year
//##################################################
const tester = ( _msg = "default msg here" ) => {

  return "from date_util.tester : " + _msg ;

};// tester
//##################################################
// all exports goes here
module.exports = {
  year,
  YYYY,
  month,
  MM,
  day,
  DD,
  hour,
  hours,
  HH,
  minute,
  minutes,
  mm,
  min,
  second,
  seconds,
  ss,
  sec,
  timestamp,
  tester,
};

