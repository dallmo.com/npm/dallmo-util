#!/bin/bash

##############################################
function separator
{ 
  line="-------------------------------"
  echo 
  echo "$line"
  echo "$1"
  echo "$line"
}
##############################################

separator "cleaning up"
echo "...removing the node_modules dir..."
rm -vrf ./node_modules/ 
rm -f yarn*
echo "done."

separator "re-install dependencies"
yarn install

separator "test result below"
node app.js
echo

