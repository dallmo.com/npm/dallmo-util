
# manual tests

1. test the package with local installation
- local/
  - cjs/
  - esm/

2. after initial publish, another test with npm installation
- npm/
  - cjs/
  - esm/

3. test with jest, configured in `package.json`, by running : 
```
yarn test
```

