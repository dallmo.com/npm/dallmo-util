"use strict";

import { setTimeout as sleep } from 'node:timers/promises';
import { math_util } from 'dallmo-util/math';

const min = 10;
const max = 100;
const sleep_time = 500;

//######################################################
const loop = async () => {

  while( true ){
    const wait = await sleep( sleep_time );
    const r = math_util.random_integer( min, max );
          console.log( r );
  }//while true

};//loop
//######################################################

loop();

