"use strict";

import { array_util } from 'dallmo-util/array';
import { setTimeout as sleep } from 'node:timers/promises';

const a = ["a","b","c"];
const sleep_interval = 500;

//###########################################################
const loop = async () => {

  while( true ){
    const wait = await sleep( sleep_interval );
    const b = array_util.random_item( a );
          console.log( b );
  }//while true

};//loop
//###########################################################

loop();

