"use strict";

const { setTimeout } = require("node:timers/promises");

const { random_integer } = require("dallmo-util/math");
const ri = random_integer;

const min = 10;
const max = 100;
const sleep_time = 500;

//######################################################
const loop = async () => {

  while( true ){
    const wait = await setTimeout( sleep_time );
    const r = ri( min, max );
          console.log( r );
  }//while true

};//loop
//######################################################

loop();

