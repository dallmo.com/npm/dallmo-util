"use strict";

/////////////////////////////////////////
const log_to_console = ( msg ) => {
  console.log( msg );
};

const temp_logger = {
  info: log_to_console,
  error: log_to_console,
};

global.logger = temp_logger;
global.logger_console = log_to_console;

/////////////////////////////////////////
module.exports = {

};
