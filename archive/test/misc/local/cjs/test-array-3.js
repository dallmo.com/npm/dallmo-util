"use strict";

const a = [1,2,3];
const b = ["a","b","c"];

let c = a.slice();

console.log( "a : ", a );
console.log( "b : ", b );
console.log( "c : ", c );

c.push("new");

console.log( "a : ", a );
console.log( "b : ", b );
console.log( "c : ", c );

let d = a;
d.push("new2");

console.log( "a : ", a );
console.log( "b : ", b );
console.log( "c : ", c );
console.log( "d : ", d );



