"use strict";

const { setTimeout }  = require('node:timers/promises');

/////////////////////////////////////////
async function action1 () {

  console.log("speaking inside action ... test 2 " );  
  await setTimeout( 1000 );

}; // action
/////////////////////////////////////////
( async () => {

    while(true){

      action1();

    }; // while loop

})();

