"use strict";

/*
  a sample udp config object
*/

const server = {
  host: "localhost",
  port: "3333",
};

const udp = {
  message: "test message in config file",
  server: server,
};

const config_obj = {
  udp: udp,
};

module.exports = {
  config_obj,
};
