"use strict";

const math_util = require("dallmo-util/math");

const r = math_util.random_integer(1,2);
const c = ( r%2 == 0 );

console.log( "r : ", r );
console.log( "c : ", c );

