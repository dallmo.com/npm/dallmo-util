"use strict";

const { random_item } = require("dallmo-util/array");
const { setTimeout }  = require('node:timers/promises');

const a = ["a","b","c"];
const sleep_interval = 500;

//###########################################################
const loop = async () => {

  while( true ){
    const wait = await setTimeout( sleep_interval );
    const b = random_item( a );
          console.log( b );
  }//while true

};//loop
//###########################################################

loop();

