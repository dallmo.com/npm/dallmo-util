"use strict";

const host = "192.168.6.36";
const port = "502";
const data = "123123sdsdf";

// while loop timer wait time, in ms
const wait_time = 1000;

const config_obj = {
  host: host,
  port: port,
  data: data,
};

/////////////////////////////////////////
const log_to_console = ( msg ) => {
  console.log( msg );
};

const temp_logger = {
  info: log_to_console,
  error: log_to_console,
};

global.logger = temp_logger;

/////////////////////////////////////////
const test_on  = "09000000000F0110270F000408233031313030300D";
const test_off = "09000000000F0110270F000408233031313030310D";

const tcp_util = require("dallmo-util/tcp");
const raw_on = tcp_util.get_hex_raw( test_on );
const raw_off = tcp_util.get_hex_raw( test_off );
/////////////////////////////////////////
module.exports = {
  config_obj,
  raw_on,
  raw_off,
  wait_time,
};
