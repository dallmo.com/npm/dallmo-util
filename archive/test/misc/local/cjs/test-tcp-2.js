"use strict";

const misc_util = require("dallmo-util/misc");
const tcp_util = require("dallmo-util/tcp");
const config = require("./test-tcp-config.js");
  const config_obj = config.config_obj;
        config_obj.wait_time = config.wait_time;
  const raw_on  = config.raw_on;
  const raw_off = config.raw_off;

  const host = config_obj.host;
  const port = config_obj.port;

/////////////////////////////////////////
/* this use "send_data_once",
    which, on each send, do all these :
      1. create connection
      2. send data
      3. close connection
*/

//////////////////////////////////////////////////////
const action = async ( config_obj ) => {

  await tcp_util.send_data( config_obj.client, config_obj.data );
  await misc_util.timer.wait( config_obj.wait_time );
  global.logger.info("waiting in loop ....................")

}; // action
//////////////////////////////////////////////////////

( async () => {

  try{

    const client = await tcp_util.socket_client_create();
          config_obj.client = client;
          
    const connect_listener = () => {
      
      global.logger.info("started connect_listener : ....");


        ( async ()=> {

          while(true){
            
            config_obj.data = raw_on;
            await action( config_obj );
            
            config_obj.data = raw_off;
            await action( config_obj );
            
          }; // while loop

        })();

        //tcp_util.socket_client_destroy( client );

    }; // connect_listener
    
    await client.connect( config_obj, connect_listener ); // client.connect
    /*
    await client.connect( config_obj, () => {
      global.logger.info("connected la")
    }); // client.connect
    */

  }catch(e){
    global.logger.error("test 2 error ... ", e.toString() );
  };

})();

