"use strict";

const { setTimeout }  = require('node:timers/promises');

///////////////////////////////////////// test 1
const action = () => {

  console.log("speaking inside action ... test 1 " );  

}; // action
/////////////////////////////////////////
( async () => {

    while(true){

      action();
      await setTimeout( 1000 );

    }; // while loop

})();

