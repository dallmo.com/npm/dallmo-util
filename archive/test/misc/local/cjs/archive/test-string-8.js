"use strict";

const a = "abcde";

console.log( a );
console.log( a.slice( 0,2 ));
console.log( a.slice( -2 ));

const string_util = require("dallmo-util/string");

const h = string_util.head;
const t = string_util.tail;

console.log( h(a,3));
console.log( t(a,3));

