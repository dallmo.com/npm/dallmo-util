"use strict";

const math_util  = require("dallmo-util/math");

const rs = math_util.random_sections;

const num_input = 50;
const num_min = 3;
const num_max = 20;
const option_obj = {
  num_min: num_min,
  num_max: num_max,
};

const option_obj3 = {
  num_min: 10,
};

const option_obj4 = {
  num_max: 10,
};


const result_array = rs( num_input, option_obj );
const result_array2 = rs( num_input );
const result_array3 = rs( num_input, option_obj3 );
const result_array4 = rs( num_input, option_obj4 );

console.log( "num_input : ", num_input );
console.log( "result : ", result_array );
console.log( "result : ", result_array2 );
console.log( "result : ", result_array3 );
console.log( "result : ", result_array4 );

