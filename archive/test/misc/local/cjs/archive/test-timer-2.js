"use strict";

const { setTimeout }  = require('node:timers/promises');

///////////////////////////////////////// test 2
const action = async () => {

  console.log("speaking inside action ... test 2 " );  
  await setTimeout( 1000 );


}; // action
/////////////////////////////////////////
( async () => {

    while(true){

      await action();

    }; // while loop

})();

