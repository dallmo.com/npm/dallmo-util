"use strict";

const udp_util = require("dallmo-util/udp");
const logger = require("./config-logger");
const config_udp = require("./config-udp");


const message_handler = ( data_in_obj ) => {
  
  const message = data_in_obj.message;
  const remote_info = data_in_obj.remote_info;
  
  global.logger.info( data_in_obj );
  global.logger.info( data_in_obj.message );
  global.logger.info( data_in_obj.remote_info );

};

udp_util.server.set_message_handler( message_handler );
udp_util.server.start( config_udp.config_obj );
