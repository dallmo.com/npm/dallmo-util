"use strict";

const string_util = require("dallmo-util/string");

const s1 = "asdfa slkjfs lkfjowiekfj lkjsdfsl";
const s2 = "a123asdfasdfadf";
const s3 = "123123123";

const test = ( _s ) => {
  console.log("--------------------------------------------");
  console.log( "s : ", _s );
  console.log( "s, type 1 : ", string_util.sentence_type( _s, 1 ));
  console.log( "s, type 2 : ", string_util.sentence_type( _s, 2 ));
};//test

test( s1 );
test( s2 );
test( s3 );

