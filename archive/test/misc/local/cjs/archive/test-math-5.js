"use strict";

const math_util = require("dallmo-util/math");
const z = math_util.add_leading_zero;

const a = 2;

console.log( z( a, 2 ));
console.log( z( a, 3 ));

