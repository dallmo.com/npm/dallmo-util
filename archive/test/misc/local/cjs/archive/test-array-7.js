"use strict";

const a = [1,2,3,4,5];

const b = a.slice( 1 );
const c = b.slice( 0, -1 );
const d = a.slice( 1 ).slice( 0, -1 );

console.log( a );
console.log( b );
console.log( c );
console.log( d );

