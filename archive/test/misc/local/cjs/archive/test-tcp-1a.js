"use strict";

const misc_util = require("dallmo-util/misc");
const tcp_util = require("dallmo-util/tcp");
const config = require("./test-tcp-config.js");
  const config_obj = config.config_obj;
  const raw_on  = config.raw_on;
  const raw_off = config.raw_off;
  config_obj.wait_time = config.wait_time;

/////////////////////////////////////////
/* this use "send_data_once",
    which, on each send, do all these :
      1. create connection
      2. send data
      3. close connection
*/

/////////////////////////////////////////
const action = async ( config_obj ) => {

  await tcp_util.send_data_once( config_obj );
  console.log("waiting in loop ....................")
  
  await misc_util.timer.wait( config.wait_time );

}; // action
/////////////////////////////////////////
( async () => {

  try{

    while(true){

      config_obj.data = raw_on;
      await action( config_obj );

      config_obj.data = raw_off;
      await action( config_obj );

    }; // while loop

  }catch(e){
    console.log("test error ... " );
  };

})();

