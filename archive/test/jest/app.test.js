'use-strict';

// the app to be tested, default to dist/app.js
const test_app = require("../../dist/app");

//////////////////////////////////////////////////////////////////////////
// customize additional files to be include here
// the sample config file
const sample_config_file = "../test/jest/sample-config.yaml";
const promise_test_app_function = test_app( sample_config_file );

// testing async code, so "return" is needed
// ref : https://jestjs.io/docs/asynchronous
test(
  "test message here ...",
  () => {
    return promise_test_app_function.then(
      config_obj => {
        expect( config_obj.key1 ).toBe('value1');
        expect( config_obj.key2.key2a ).toBe('value2a');
    }); // promise
  });// test


